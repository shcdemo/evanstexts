#/bin/sh
topdir="$(dirname "$0")/../"
olddir=`pwd`
cd "$topdir"
git diff --name-only HEAD~1 | xargs -I {} sh -c 'cd {}; git diff --name-only HEAD~1; cd ../../'
cd "$olddir"
